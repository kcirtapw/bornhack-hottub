import twitter



RequiredEnvVars = ["CONSUMER_KEY", "CONSUMER_SECRET", "ACCESS_TOKEN_KEY", "ACCESS_TOKEN_SECRET"]


def create_api(consumer_key, consumer_secret, access_token_key, access_token_secret):
    return twitter.Api(consumer_key, consumer_secret, access_token_key, access_token_secret)


def get_tokens_from_environment():
    import os
    EnvVarsValues = {}
    for var in RequiredEnvVars:
        try:
            EnvVarsValues[var] = os.environ[var]
        except KeyError:
            raise RuntimeError(("Required token {var_name} not found in environment variables!\n\n" +
                               "Following environment variables are set: {env_vars}").format(
                    var_name=var, env_vars=", ".join(os.environ.keys())))
    return EnvVarsValues


def setup_twitter():
    tokens = get_tokens_from_environment()
    return create_api(*tokens.values())





