from mqtt import client, route_subscription
from datetime import datetime, timedelta
from twitter_api import setup_twitter
import sys

broker_address = "test.mosquitto.org"


last_publish = datetime.fromtimestamp(0)


@route_subscription("/dk/bornhack/hottub/water_temp")
def handle_temp(temp_reading):
    global last_publish
    print("received current temp: {}".format(temp_reading), end="")

    if float(temp_reading) <= 37.0:  # still to cold
        print(", but it is still to cold")
    else:
        now = datetime.now()
        if last_publish + timedelta(hours=1) > now:  # dont spam
            print(", but we recently sent a tweet")
        else:  # okay, send a tweet
            last_publish = now
            tweet_current_temp(temp_reading)


def tweet_current_temp(temp):
    print("Tweeting current temperature: {}".format(temp))
    tweet = twitter_api.PostUpdate("The #Bornhack hot tub is currently at {temp}°C".format(temp=temp))
    if not tweet:
        print("sending tweet failed...", file=sys.stderr)


if __name__ == "__main__":
    print("setup twitter")
    twitter_api = setup_twitter()
    print("connecting to broker:", broker_address)
    client.connect(broker_address)
    print("connected ...")

    try:
        print("starting the loop...")
        client.loop_forever()
    except:
        client.loop_stop(force=True)
