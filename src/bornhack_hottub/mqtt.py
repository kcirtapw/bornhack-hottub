import paho.mqtt.client as mqtt

import sys


client = mqtt.Client("Bornhack Hottub Twitter Bot")  #create new instance


def route_message(client, userdata, message):
    try:
        handle_method = route_message.routing_map[message.topic][0]
    except KeyError:
        print("received message for unknown topic: {topic}".format(topic=message.topic), file=sys.stderr)
        return
    try:
        decoded_payload = str(message.payload.decode("utf-8"))
        handle_method(decoded_payload)
    except Exception as e:
        print(e)
        return
route_message.routing_map = {}


def route_subscription(topic, qos=0):
    def _decorator(func):
        route_message.routing_map[topic] = (func, qos)
        return func
    return _decorator


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    for topic in route_message.routing_map.keys():
        print("subscribing to topic: {topic}".format(topic=topic))
        client.subscribe(topic)


client.on_message = route_message
client.on_connect = on_connect