# setup

´´´
$ virtualenv -p python3 .venv
...
$ .venv/bin/pip install -r requirements.txt
´´´

# example startup

´´´
$ export CONSUMER_KEY=foooo
$ export CONSUMER_SECRET=baaaar
$ export ACCESS_TOKEN_KEY=baaaaz
$ export ACCESS_TOKEN_SECRET=shiizle
$ export PYTHONPATH=".src:$PYTHONPATH"
$ .venv/bin/python -m bornhack_hottub.bot
´´´
